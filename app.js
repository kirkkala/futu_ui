/**
 * FutuUI app.js
 *   - the cleaner approach that was rejected due to the time constraint
 *   Stored for later ref -> is anyone wants to make it more awesome
 */

var FutuUI = FutuUI || {};

(function($, textToVoice, voiceParser) {
  function FutuUI() {
    this.state = {
      balance: 0
    };

    this.scripts = {
      messages: {
        welcome: 'Hello! Welcome to the the ATM machine from the future',
        command: 'What would you like to do? Say withdraw or balance',
        withdraw: 'Say how much you want to withdraw',
        balance: 'Your account balance is',
        unRecognizedCommand: 'Sorry, I did not catch what command you mean. Please repeat!',
        error: 'Something wrong happens! We are trying to debug it. Please try again in a moment',
        networkError: 'Shit! I can not believe that you do not even have internet connection in this century',
      },
      voiceCommands: {
        selectCommand: {
          'I want to :command': this.handleCommand,
        },
        withdraw: {
          'withdraw :amount': this.handleWithdraw,
        },

      }
    };
  }


  /**
   * Say something from text.
   */
  FutuUI.prototype.say = function(text) {
    if (textToVoice) {
      textToVoice.speak(text);
    }
  }

  FutuUI.prototype.voiceCommand = function(commands) {
    if (voiceParser) {
      voiceParser.removeCommands();
      voiceParser.abort();

      voiceParser.addCommands(commands);
      voiceParser.start();
    }
  }

  FutuUI.prototype.chooseCommand = function() {
    this.say(this.scripts.messages.command);
  }

  /**
   * Withdraw money.
   */
  FutuUI.prototype.withdraw = function() {
    this.say(this.scripts.messages.withdraw);
  }

  FutuUI.prototype.balance = function() {
    var message = this.scripts.messages.balance + ' ' + this.state.balance;
    this.say(this.scripts.messages.balance);
  }

  FutuUI.prototype.error = function() {
    this.say(this.scripts.messages.error);
  }

  FutuUI.prototype.networkError = function() {
    this.say(this.scripts.messages.networkError);
  }

  FutuUI.prototype.init = function() {
    if (voiceParser) {
      var self = this;

      voiceParser.addCallback('error', function(error) {
        console.log(error);
        self.error();
      });

      voiceParser.addCallback('errorNetwork', function() {
        self.networkError();
      });
    }
  }

  FutuUI.prototype.welcome = function() {
    this.say(this.scripts.messages.welcome);
  }

  /**
   * Run everything.
   */
  FutuUI.prototype.run = function() {
    this.init();
    this.welcome();
    this.voiceCommand(this.scripts.voiceCommands.selectCommand);
  }

  // Run everything
  new FutuUI().run();
})(jQuery, responsiveVoice, annyang);

