if (annyang) {
  var commands = {
    'Hello there': function() {
      $('.bank').fadeOut('slow', function(e) {
        $('.hi')
          .html('Welcome Mr John Doe. How can I help you today?')
          .fadeIn('slow', function(e) {
            responsiveVoice.speak($('.hi').html());
        });
      });
    },
    'I would like to transfer money': function() {
      $('.hi').fadeOut('slow', function(e) {
        $('.howmuch')
          .html('Absolutely. How much would you like to transfer?')
          .fadeIn('slow', function(e) {
            responsiveVoice.speak($('.howmuch').html());
        });
      });
    },
    'Transfer :amount': function(amount) {
      $('.howmuch').fadeOut('slow', function(e) {
        $('.transfer')
          .html('Sure. I just transferred $ ')
          .append(amount)
          .fadeIn('slow', function(e) {
            responsiveVoice.speak($('.transfer').html());
        });
      });
    },
    'Thank you': function(amount) {
      $('.transfer').fadeOut('slow', function(e) {
        $('.thanks')
          .html('You are welcome. Is there anything else I can help you with?')
          .fadeIn('slow', function(e) {
            responsiveVoice.speak($('.thanks').html());
        });
      });
    },
    'No thanks': function(amount) {
      $('.thanks').fadeOut('slow', function(e) {
        $('.no')
          .html('Good Bye.')
          .fadeIn('slow', function(e) {
            responsiveVoice.speak($('.no').html());
        });
      });
    },
  };

  annyang.addCommands(commands);
  annyang.start();

  // Trigger the "voicevisual"
  annyang.addCallback('result', function() {
    timeout();
  });
}

// Wobble the dots
function timeout() {
  var dotB = $('#dots .blue'),
      dotR = $('#dots .red'),
      dotY = $('#dots .yellow'),
      dotG = $('#dots .green');

  dotB.css("animation","updown 1.2s infinite ease-in-out alternate");
  dotR.css("animation","updown 1.2s 0.2s infinite ease-in-out alternate");
  dotY.css("animation","updown 1.2s 0.4s infinite ease-in-out alternate");
  dotG.css("animation","updown 1.2s 0.6s infinite ease-in-out alternate");
  setTimeout(function(){
    dotB.css("animation","sound-1 1.4s");
    dotR.css("animation","sound-2 1.4s 0.25s");
    dotY.css("animation","sound-1 1.4s 0.10s");
    dotG.css("animation","sound-2 1.4s 0.15s");
    setTimeout(function(){
      dotB.css("animation","finalani 0.4s");
      dotR.css("animation","finalani 0.4s 0.05s");
      dotY.css("animation","finalani 0.4s 0.1s");
      dotG.css("animation","finalani 0.4s 0.15s");
      setTimeout(function(){
      }, 550);
    }, 1190);
  }, 100);
}
